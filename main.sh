#!/bin/bash

PROJECT_NAME=libqdedup

if [ "$#" -ne 8 ]; then
	echo "Usage: $0 NASX86_STORMGMT ARCH OS BRANCH MAJOR_VERSION MINOR_VERSION MICRO_VERSION PATCH_VERSION" >&2
	exit 1
fi

NASX86_STORMGMT="$1"
ARCH="$2"
OS="$3"
BRANCH="$4"
MAJOR_VERSION="$5"
MINOR_VERSION="$6"
MICRO_VERSION="$7"
PATCH_VERSION="$8"

BUILD_SRC="${PROJECT_NAME}_${OS}_${ARCH}_${MICRO_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}.${PATCH_VERSION}"
CONTAINER="${USER}-${ARCH}-${BUILD_SRC}"
DOCKER_EXEC="docker exec -i ${CONTAINER} bash -c"

function update_qnap_docker_deploy()
{
	if [ ! -d docker_deploy ]; then
		git clone http://172.17.23.195:10080/QNAP/docker_deploy.git docker_deploy || return 1
		sed -i 's/`date +%s`/$2/g' docker_deploy/container_setup.sh || return 1
		sed -i 's/-ti/-tid/g' docker_deploy/container_setup.sh || return 1
		sed -i 's/-v \/mnt\/pub:\/mnt\/pub:ro/-v \/mnt\/pub:\/mnt\/pub:ro -v \/mnt\/data\/$USER:\/root\/extension/g' docker_deploy/container_setup.sh || return 1
		sed -i 's/--rm//g' docker_deploy/container_setup.sh || return 1
	fi
}

function create_container()
{
	local OS="$1"
	local ARCH="$2"
	local BUILD_SRC="$3"

	if [ x"${OS}" = x"QTS_X86_64" ] ||
           [ x"${OS}" = x"QTS_ARMAL_32" ] ||
           [ x"${OS}" = x"QTS_ARMMS_32" ] ||
	   [ x"${OS}" = x"QTS_ARM_64" ]; then
		sh docker_deploy/container_setup.sh ${ARCH} ${BUILD_SRC} || return 1
	elif [ x"${OS}" = x"LINUX64_CENTOS" ];
		sh centos_docker/container_setup.sh ${ARCH} ${BUILD_SRC} || exit 1
	else
		sh ubuntu_docker/container_setup.sh ${ARCH} ${BUILD_SRC} || return 1
	
	fi
}

function destroy_container()
{
	local CONTAINER="$1"

	docker rm -f "${CONTAINER}"
}

function pull_codes()
{
	local NASX86_STORMGMT="$1"
	local BUILD_SRC="$2"
	local BRANCH="$3"

	echo "Remove old codes: ${NASX86_STORMGMT}/${BUILD_SRC}"
	${DOCKER_EXEC} "[ ! -d ${NASX86_STORMGMT}/${BUILD_SRC} ] || rm -rf ${NASX86_STORMGMT}/${BUILD_SRC}"

	echo "Clone codes"
	${DOCKER_EXEC} "cd ${NASX86_STORMGMT} && git clone ssh://git@10.20.91.8:10022/dereksu/qdedup.git ${BUILD_SRC}" || return 1

	echo "Checkout to speficied branch: ${BUILD_SRC}"
	${DOCKER_EXEC} "cd ${NASX86_STORMGMT}/${BUILD_SRC} && git checkout -b building origin/${BRANCH}" || return 1
}

function remove_codes()
{
	local NASX86_STORMGMT="$1"
	local BUILD_SRC="$2"

	echo "Remove codes: ${NASX86_STORMGMT}/${BUILD_SRC}"
	${DOCKER_EXEC} "[ ! -d ${NASX86_STORMGMT}/${BUILD_SRC} ] || rm -rf ${NASX86_STORMGMT}/${BUILD_SRC}" || return 1
}

function build_codes()
{
	local NASX86_STORMGMT="$1"
	local BUILD_SRC="$2"
	local OS="$3"
	local MAJOR_VERSION="$4"
	local MINOR_VERSION="$5"
	local MICRO_VERSION="$6"
	local PATCH_VERSION="$7"

	${DOCKER_EXEC} "cd ${NASX86_STORMGMT}/${BUILD_SRC} && make QDEDUP_OS=${OS} QDEDUP_BUILD_MICROVERSION=${MICRO_VERSION} QDEDUP_BUILD_PATCHVERSION=${PATCH_VERSION}" || return 1
	${DOCKER_EXEC} "cd ${NASX86_STORMGMT}/${BUILD_SRC} && make mktgz QDEDUP_OS=${OS} QDEDUP_BUILD_MICROVERSION=${MICRO_VERSION} QDEDUP_BUILD_PATCHVERSION=${PATCH_VERSION}" || return 1
}

function copyout_artifacts()
{
	local NASX86_STORMGMT="$1"
	local BUILD_SRC="$2"

	docker cp ${CONTAINER}:${NASX86_STORMGMT}/${BUILD_SRC}/QPKG/build artifacts || return 1
}

##############################################
# Main locic
##############################################
update_qnap_docker_deploy
if [  $? != 0 ]; then
	echo "Failed to update QNAP docker deploy script"
	exit 1
fi

create_container "${OS}" "${ARCH}" "${BUILD_SRC}"
if [ $? != 0 ]; then
	echo "Failed to create container: ${ARCH}_${BUILD_SRC}"
	exit 1
fi

pull_codes "${NASX86_STORMGMT}" "${BUILD_SRC}" "${BRANCH}"
if [ $? != 0 ]; then
	destroy_container "${CONTAINER}"
	echo "Failed to pull codes"
	exit 1
fi

build_codes "${NASX86_STORMGMT}" "${BUILD_SRC}" "${OS}" "${MAJOR_VERSION}" "${MINOR_VERSION}" "${MICRO_VERSION}" "${PATCH_VERSION}"
if [ $? != 0 ]; then
	destroy_container "${CONTAINER}"
	echo "Failed to build codes"
	exit 1
fi

copyout_artifacts "${NASX86_STORMGMT}" "${BUILD_SRC}"
if [ $? != 0 ]; then
	destroy_container "${CONTAINER}"
	echo "Failed to copy artifacts out"
	exit 1
fi

remove_codes "${NASX86_STORMGMT}" "${BUILD_SRC}"
if [ $? != 0 ]; then
	destroy_container "${CONTAINER}"
	echo "Failed to remove code"
	exit 1
fi

destroy_container "${CONTAINER}"
